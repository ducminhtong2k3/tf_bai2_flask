from datetime import timedelta
from app.instance.config import user, password, host, database, port

SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://{0}:{1}@{2}:{3}/{4}'.format(
        user, password, host, port, database
)

SQLALCHEMY_TRACK_MODIFICATIONS = False

DEBUG = True


JWT_TOKEN_LOCATION = 'cookies'
JWT_COOKIE_CSRF_PROTECT = False
JWT_ACCESS_TOKEN_EXPIRES = timedelta(seconds=300)
