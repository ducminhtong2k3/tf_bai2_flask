from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_caching import Cache
from flask_cors import CORS
from flask_jwt_extended import JWTManager


db = SQLAlchemy()

jwt = JWTManager()

cache = Cache(config={'CACHE_TYPE': 'SimpleCache'})


def create_app():

    app = Flask(__name__)

    app.config.from_pyfile('config.py')
    app.config.from_pyfile('instance/config.py')

    db.init_app(app)

    jwt.init_app(app)

    cache.init_app(app)

    CORS(app)

    from app.main.api import auth
    app.register_blueprint(auth.bp)

    from app.main.api import services
    app.register_blueprint(services.bp)
    app.add_url_rule('/', endpoint='index')

    from app.main.error_handle.error_handle import unauthenticated
    app.register_error_handler(401, unauthenticated)

    return app
