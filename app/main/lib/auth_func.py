import re
import uuid

from flask import make_response
from flask_jwt_extended import (
    create_access_token,
    set_access_cookies, unset_jwt_cookies,
)
from werkzeug.security import check_password_hash, generate_password_hash

from app import db
from app.main.model.user import User


def check_valid_username(username):
    """Username only contains letters, numbers and underscores, between 5 and 30 characters"""
    pattern = re.compile("^[a-zA-Z_0-9]{5,30}$")
    return (False, True)[pattern.match(username) is not None]


def generate_access_tokens(user_id):
    access_token = create_access_token(identity=str(user_id))
    resp = make_response({"message": "Token is generated. Ready to access"}, 302)
    set_access_cookies(resp, access_token)
    return resp


def unset_jwt():
    resp = make_response({"message": "Logout successfully"}, 302)
    unset_jwt_cookies(resp)
    return resp


def signup_func(username, password):

    if check_valid_username(username) is False:
        return {"message": "Invalid username"}

    user = User.query.filter_by(name=username).first()

    if user:
        return make_response('User already exists', 403)

    new_user = User(name=username, password=generate_password_hash(password),
                    public_id=str(uuid.uuid4()))

    db.session.add(new_user)
    db.session.commit()

    return {"message": "Signup successfully"}


def login_func(username, password):
    
    if check_valid_username(username) is False:
        return {"message": "Invalid username"}

    user = User.query.filter_by(name=username).first()

    if user is None:
        return make_response('Username incorrect', 403, {'Authentication': 'Login required'})

    if check_password_hash(user.password, password):
        # Generate JWT in cookie
        return generate_access_tokens(user_id=user.public_id)

    return make_response('Password incorrect', 403, {'Authentication': 'Login required'})
