from app import db
from app.main.model.article import Article


def create_func(title, content):
    article = Article(title=title, content=content)

    db.session.add(article)
    db.session.commit()

    return article.schema_json()


def update_func(title, content, article_id):
    article = Article.query.get_or_404(article_id)

    if article is None:
        return {"message": "ID not found"}

    if title is not None:
        article.title = title

    if content is not None:
        article.content = content

    db.session.add(article)
    db.session.commit()

    return {"message": "Update Success"}


def delete_func(article_id):
    article = Article.query.get_or_404(article_id)

    if article is None:
        return {"message": "ID not found"}

    db.session.delete(article)
    db.session.commit()


def search_func(title):
    articles = Article.query.filter(Article.title.contains(title)).order_by(Article.id).all()
    if articles is None:
        return {"message": "Cannot found"}
    return {"Articles": list(x.schema_json() for x in articles)}
