from marshmallow import fields, Schema, ValidationError, EXCLUDE, validates


class ArticleSchemas(Schema):
    title = fields.Str(required=True)
    content = fields.Str(required=True)

    @validates("title")
    def validates_title(self, value):
        if len(value) < 1:
            raise ValidationError("Data cannot be empty")
        if not any(c.isupper() for c in value):
            raise ValidationError("Data must contain upper case")
        if not any(c.islower() for c in value):
            raise ValidationError("Data must contain lower case")

    @validates("content")
    def validates_content(self, value):
        if len(value) < 1:
            raise ValidationError("Data cannot be empty")
        if not any(c.isupper() for c in value):
            raise ValidationError("Data must contain upper case")
        if not any(c.islower() for c in value):
            raise ValidationError("Data must contain lower case")

    class Meta:
        strict = True
        unknown = EXCLUDE
