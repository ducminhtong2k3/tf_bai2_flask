from flask import Blueprint, request, redirect, url_for
from flask_jwt_extended import jwt_required
from marshmallow import ValidationError

from app.main.model.article import Article
from app import cache
from app.main.lib.services_func import create_func, update_func, delete_func, search_func
from app.main.schemas.article import ArticleSchemas

bp = Blueprint('service', __name__)


@bp.route('/')
# @jwt_required()
def index():
    articles = Article.query.order_by(Article.id.desc()).limit(6).all()
    return {"Articles": list(x.schema_json() for x in articles)}


@bp.route('/page')
@cache.cached(timeout=10000)
def index_paging():
    data = request.get_json()
    articles = Article.query.order_by(Article.id.desc()).paginate(page=int(data['page']), per_page=int(data['size']))
    return {"Articles": list(x.schema_json() for x in articles.items)}


@bp.route('/p<int:page_id>')
def index_paging_by_id(page_id):
    articles = Article.query.order_by(Article.id.desc()).paginate(page=page_id, per_page=6)
    return {"Articles": list(x.schema_json() for x in articles.items)}


@bp.route('/<int:id_article>')
@cache.cached(timeout=10000)
def index_article(id_article):
    article = Article.query.filter_by(id=id_article).first()
    if article is None:
        return "ID not found!!"
    return article.schema_json()


@bp.route('/create', methods=['POST', ])
@jwt_required()
def create():
    if request.method == 'POST':
        data = request.get_json()
        try:
            data = ArticleSchemas().load(data)
            return create_func(data['title'], data['content'])
        except ValidationError as e:
            return e.messages


@bp.route('/update/<int:article_id>', methods=['PUT', 'GET'])
@jwt_required()
def update(article_id):
    if request.method == 'PUT':
        data = request.get_json()
        try:
            data = ArticleSchemas().load(data)
            return update_func(data['title'], data['content'], article_id)
        except ValidationError as e:
            return e.messages


@bp.route('/delete/<int:article_id>')
@jwt_required()
def delete(article_id):
    delete_func(article_id)
    return redirect(url_for('index'))


@bp.route('/search', methods=['POST', ])
def search():
    if request.method == 'POST':
        data = request.get_json()
        try:
            data = ArticleSchemas().load(data)
            return search_func(data['title'])
        except ValidationError as e:
            return e.messages
