from flask import Blueprint, request, make_response
from flask_jwt_extended import unset_jwt_cookies, unset_access_cookies

from app import jwt
from app.main.lib.auth_func import unset_jwt, signup_func, login_func

bp = Blueprint('auth', __name__)


@jwt.unauthorized_loader
def unauthorized_callback(callback):
    return make_response({"message": "Login to access"}, 302)


@jwt.invalid_token_loader
def invalid_token_callback(callback):
    # Invalid Fresh/Non-Fresh Access token in auth header
    resp = make_response({"message": "Invalid token"})
    unset_jwt_cookies(resp)
    return resp, 302


@jwt.expired_token_loader
def expired_token_callback(callback, data):
    # Expired auth header
    resp = make_response({"message": "Token is expired"})
    unset_access_cookies(resp)
    return resp, 302


@bp.route('/signup', methods=['POST', ])
def signup():
    data = request.get_json()

    if not data or not data['name'] or not data['password']:
        return make_response('Could not signup', 403, {'Authentication': 'Data required'})

    return signup_func(data['name'], data['password'])


@bp.route('/login', methods=['POST', ])
def login():
    auth = request.get_json()

    if not auth or not auth['name'] or not auth['password']:
        return make_response('Could not verify', 403, {'Authentication': 'Login required'})

    return login_func(auth['name'], auth['password'])


@bp.route('/logout')
def logout():
    return unset_jwt(), 302
