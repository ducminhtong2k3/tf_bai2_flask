from app import db
from sqlalchemy import func


class Article(db.Model):
    __tablename__ = "data_table"
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.Text)
    content = db.Column(db.Text)
    created_at = db.Column(db.DateTime(timezone=True), server_default=func.now())

    def __repr__(self):
        return f'<Article {self.id}>'

    def schema_json(self):
        return {"id": self.id, "title": self.title,
                "content": self.content, "created_at": self.created_at}
