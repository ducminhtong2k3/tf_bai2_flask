from app import db


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False, unique=True)
    password = db.Column(db.Text)
    public_id = db.Column(db.Text)

    def __repr__(self):
        return f'<User {self.id}>'

    def schema_json(self):
        return {"id": self.id, "name": self.name, "password": self.password}
