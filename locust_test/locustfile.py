from locust import HttpUser, task


class WebUser(HttpUser):

    @task
    def get_index(self):
        self.client.get(url='/')

    @task
    def get_article(self):
        for id in range(1, 300):
            self.client.get(url=f'/{id}')

    @task
    def put_update(self):
        self.client.post(url='/login', json={
            'name': 'hoang',
            'password': 'hoang'
        })
        self.client.put(url='/update/300', json={
            'title': 'test',
            'context': 'test'
        })
